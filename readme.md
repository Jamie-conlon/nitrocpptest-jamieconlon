# Nitro C++ Proficiency Test - Jamie Conlon

## Requirements
  * [CMake](https://cmake.org/)
  * C++ Compiler 

## Build Instructions
  1. Download the project.
  2. In terminal/command prompt, navigate to project root directory.
  3. Run: ` cmake . ` to generate the build files.
  4. Run: ` cmake --build . ` to build the project

#### External libraries

- [nlohmann/json](https://github.com/nlohmann/json): json  
- [Catch2](https://github.com/catchorg/Catch2): unit testing 

Both libraries are single header files. I have included them in the project to keep things simple. These are located in `project_root_dir/ext/`

## Run Instructions

#### Windows

The executable and test executable will be located in `project_root_dir/bin/Debug/`

- To run on executable with the test data, enter: ` nitro ../../testdata/test1.json `

- To run unit tests enter: `nitro_tests`

#### Mac/Linux

The executable and test executable will be located in `project_root_dir/bin/`

- To run on executable with the test data, enter: ` ./nitro ../testdata/test1.json `

- To run unit tests enter: `./nitro_tests`