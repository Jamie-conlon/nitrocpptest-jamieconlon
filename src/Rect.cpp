/*
 * Rect.cpp
 */

#include "Rect.h"
#include <iostream>
#include <algorithm>

namespace nitro {

	Rect::Rect(int x, int y, int w, int h)
		: x_pos(x), y_pos(y), width(w), height(h)
	{
		if (width < 1)
			throw std::invalid_argument("width cannot be less than 1");
		if (height < 1)
			throw std::invalid_argument("height cannot be less than 1");

		// check for max dimension values
		if (x_pos > kMaxRectDimension)
			throw std::invalid_argument("x coodinate cannot exceed 1000000");
		if (y_pos > kMaxRectDimension)
			throw std::invalid_argument("y coodinate cannot exceed 1000000");
		if (width > kMaxRectDimension)
			throw std::invalid_argument("width cannot exceed 1000000");
		if (height > kMaxRectDimension)
			throw std::invalid_argument("height cannot exceed 1000000");
	}

	bool Rect::intersects(Rect& rect)
	{
		if ((x_pos < (rect.x_pos + rect.width)) &&
			((x_pos + width) > rect.x_pos) &&
			(y_pos < (rect.y_pos + rect.height)) &&
			((y_pos + height) > rect.y_pos))
		{
			return true;
		}
		return false;
	}

	Rect Rect::intersection(Rect& rect)
	{
		if (!this->intersects(rect)) // **** Do we need this? check in unit tests
			return Rect(0, 0, 0, 0);

		// insert all hoizontal and vertical values into two arrays
		int xValues[] = { x_pos, rect.x_pos, x_pos + width, rect.x_pos + rect.width };
		int yValues[] = { y_pos, rect.y_pos, y_pos + height, rect.y_pos + rect.height };

		//sort arrays
		std::sort(std::begin(xValues), std::end(xValues));
		std::sort(std::begin(yValues), std::end(yValues));

		// create new Rect from the two inner-most values from the x and y arrays
		Rect new_rect(xValues[1], yValues[1], xValues[2] - xValues[1], yValues[2] - yValues[1]);

		// insert overlaps from current Rects into new Rect
		new_rect.overlaps.insert(overlaps.begin(), overlaps.end());
		new_rect.overlaps.insert(rect.overlaps.begin(), rect.overlaps.end());

		return new_rect;
	}
}