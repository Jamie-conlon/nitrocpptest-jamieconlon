/*
 * CollisionDetector.cpp
 */

#include "CollisionDetector.h"
#include <algorithm>
#include <iostream>
#include <vector>

namespace nitro {

	void CollisionDetector::detectIntersections(std::vector<Rect>& input_rects)
	{
		std::vector<Rect> temp_rects;

		if (!input_rects.empty())
		{
			for (auto i = 0; i != input_rects.size() - 1; ++i) 
			{
				for (auto j = i+1; j != input_rects.size(); ++j)
				{
					if (input_rects[i].intersects(input_rects[j]))
					{
						Rect intersection = input_rects[i].intersection(input_rects[j]);
						if (!checkIfDuplicate(intersection.overlaps, temp_rects))
							temp_rects.push_back(intersection);
					}
				}
			}
			for (auto &it : temp_rects)
				intersect_rects.push_back(it); // put found intersections into main intersection vector

			detectIntersections(temp_rects); // recurse here
		}
	}

	void CollisionDetector::outputIntersections()
	{
		std::cout << "\nIntersections:\n";
		for (auto rect : intersect_rects)
		{
			std::cout << "\tBetween rectangle ";
			size_t overlapCounter = 0;
			for (auto overlap : rect.overlaps)
			{
				std::cout << overlap;
				if(overlapCounter < rect.overlaps.size() - 2)
					std::cout << ", ";
				else if (overlapCounter < rect.overlaps.size() - 1)
					std::cout << " and ";
				overlapCounter++;
			}
			std::cout << " at (" << rect.x_pos << "," << rect.y_pos << "), w=" << rect.width << ", h=" << rect.height << std::endl;
		}
	}

	std::vector<Rect> CollisionDetector::getAllIntersections() 
	{
		return intersect_rects;
	}

	bool CollisionDetector::checkIfDuplicate(std::set<int> &s1, std::vector<Rect> &rects)
	{
		for (auto it : rects)
		{
			if (s1 == it.overlaps)
				return true;
		}
		return false;
	}
}