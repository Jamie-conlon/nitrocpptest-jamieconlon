/*
 * main.cpp
 */

#include "InputValidator.h"
#include "JSONParser.h"
#include "CollisionDetector.h"
#include "Rect.h"
#include <iostream>

int main(int argc, char* argv[])
{
	nitro::InputValidator valiator;
	std::vector<nitro::Rect> input_rects;
	std::string filename;

	// Validate filename
	try{	
		filename = valiator.validate(argv[1]);
	}
	catch (std::invalid_argument err) {
		std::cerr << err.what() << std::endl;
		return 0;
	}

	// Parse JSON file
	if (!filename.empty())
	{
		nitro::JSONParser parser;
		try {
			parser.FileToJSON(filename);
			input_rects = parser.JSONToRects();
			parser.OutputRects();
		}
		catch (std::invalid_argument err) {
			std::cerr << err.what() << std::endl;
			return 0;
		}
	}

	// Detect all intersections
	if (!input_rects.empty())
	{
		nitro::CollisionDetector detector;

		detector.detectIntersections(input_rects);
		detector.outputIntersections();
	}

	return 0;
}
