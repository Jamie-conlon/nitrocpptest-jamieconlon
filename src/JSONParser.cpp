/*
 * JSONParser.cpp
 */

#include "JSONParser.h"
#include "Rect.h"
#include "json.hpp"
#include <fstream>
#include <iostream>
#include <stdexcept>

namespace nitro {

	void JSONParser::FileToJSON(std::string filename) 
	{
		std::ifstream input_file(filename);
		if (input_file.is_open())
		{
			try {
				input_file >> json_object; // deserialize from input_file
			}
			catch (nlohmann::detail::parse_error) {
				std::cerr << "ERROR: Please ensure JSON is formatted correctly. " << std::endl;
			}
		}
		else
			throw std::invalid_argument("ERROR: Can't open file.");
	}

	std::vector<Rect> JSONParser::JSONToRects() 
	{
		auto rects_section = json_object["rects"];

		for (auto i=0; i != rects_section.size() && i != kMaxRects; ++i) {

			try {
				rects.push_back(rectFromJSON(rects_section[i]));
				rects.back().overlaps.insert(i+1); // In the case of input rects, overlaps is used as a rect id.
			}
			catch (nlohmann::detail::type_error) {
				std::cerr << "ERROR: Rectangle " << i+1 << " contains invalid values. Skipping..." << std::endl;
			}
			catch (std::invalid_argument err) {
				std::cerr << "ERROR: Rectangle " << i+1 << " " << err.what() << " . Skipping..." << std::endl;
			}
		}
		return rects;
	}
	
	void JSONParser::OutputRects()
	{
		if (!rects.empty())
		{
			std::cout << std::endl << "Valid Input:" << std::endl;
			for (auto &rectIt : rects)
				std::cout << "\t"<< (*rectIt.overlaps.begin()) << ": Rectangle at (" << rectIt.x_pos << "," << rectIt.y_pos << "), w=" << rectIt.width << ", h=" << rectIt.height << "." << std::endl;
		}
		else {
			std::cout << "No rectangles found." << std::endl;
		}
	}

	Rect JSONParser::rectFromJSON(const nlohmann::json &j) 
	{
		int x, y, w, h;

		j.at("x").get_to(x);
		j.at("y").get_to(y);
		j.at("w").get_to(w);
		j.at("h").get_to(h);
		
		return Rect(x, y, w, h);
	}
}