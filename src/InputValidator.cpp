/*
 * InputValidator.cpp
 */

#include "InputValidator.h"
#include <fstream>

namespace nitro {

	std::string InputValidator::validate(const char *progArg)
	{
		if (progArg) // is there an argument supplied
		{
			std::string program_argument = progArg; 
			if (fileExists(program_argument) && isJSONFile(program_argument))
				return program_argument;

		} else {
			throw std::invalid_argument("ERROR: No argument supplied");
		}
		return std::string(); // return empty string
	}

	bool InputValidator::fileExists(const std::string filename)
	{
		 std::ifstream infile(filename); 
   		 if (!infile.good()) {
			 throw std::invalid_argument("ERROR: File does not exist");
			return false;
		}
		return true;
	}

	bool InputValidator::isJSONFile(const std::string jsonfile)
	{
		std::string ext = jsonfile.substr(jsonfile.find_last_of(".")); // get file extension
		for (auto &c: ext) c = toupper(c); // make uppercase

		if(ext != std::string(".JSON")) {
			throw std::invalid_argument("ERROR: Not a JSON File");
			return false;
		}
		return true;
	}
}