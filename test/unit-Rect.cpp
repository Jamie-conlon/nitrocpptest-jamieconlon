/*
 * unit-JsonParser.cpp
 */

#include "catch.hpp"
#include "Rect.h"

namespace nitro_test {

	TEST_CASE("Create valid rect", "[Rect]") 
	{	
		nitro::Rect rect(10, 10, 10, 10);

		REQUIRE(rect.x_pos == 10);
		REQUIRE(rect.y_pos == 10);
		REQUIRE(rect.height == 10);
		REQUIRE(rect.width == 10);
	}

	TEST_CASE("Throw exeception if constructor width and height parameters are < 0", "[Rect]") 
	{
		REQUIRE_THROWS_AS(nitro::Rect(10, 10, -10, 10), std::invalid_argument);
		REQUIRE_THROWS_AS(nitro::Rect(10, 10, 10, -10), std::invalid_argument);
	}

	TEST_CASE("Throw exeception if constructor parameters are > max ", "[Rect]") 
	{
		REQUIRE_THROWS_AS(nitro::Rect(1000001, 10, 10, 10), std::invalid_argument);
		REQUIRE_THROWS_AS(nitro::Rect(10, 1000001, 10, 10), std::invalid_argument);
		REQUIRE_THROWS_AS(nitro::Rect(10, 10, 1000001, 10), std::invalid_argument);
		REQUIRE_THROWS_AS(nitro::Rect(10, 10, 10, 1000001), std::invalid_argument);
	}
	
	TEST_CASE("Intersection exists", "[Rect]") 
	{
		nitro::Rect rect1(10, 10, 10, 10);
		nitro::Rect rect2(5, 5, 10, 10);
		
		REQUIRE(rect1.intersects(rect2));
	}

	TEST_CASE("Intersection exists - minus position", "[Rect]")
	{
		nitro::Rect rect1(-10, -10, 10, 10);
		nitro::Rect rect2(-5, -5, 10, 10);

		REQUIRE(rect1.intersects(rect2));
	}

	TEST_CASE("Intersection exists - full overlap", "[Rect]")
	{
		nitro::Rect rect1(0, 0, 20, 20);
		nitro::Rect rect2(5, 5, 5, 5);

		REQUIRE(rect1.intersects(rect2));
	}

	TEST_CASE("Intersection does not exist", "[Rect]")
	{
		nitro::Rect rect1(10, 10, 10, 10);
		nitro::Rect rect2(25, 25, 10, 10);

		REQUIRE_FALSE(rect1.intersects(rect2));
	}
	
	TEST_CASE("Intersection does not exist - Edges touching", "[Rect]")
	{
		nitro::Rect rect1(10, 10, 10, 10);
		nitro::Rect rect2(20, 10, 10, 10);

		REQUIRE_FALSE(rect1.intersects(rect2));
	}

	TEST_CASE("Get intersection values - intersection exists", "[Rect]")
	{
		nitro::Rect rect1(10, 10, 10, 10);
		nitro::Rect rect2(5, 5, 10, 10);

		nitro::Rect resultRect = rect1.intersection(rect2);

		CHECK(resultRect.x_pos == 10);
		CHECK(resultRect.y_pos == 10);
		CHECK(resultRect.height == 5);
		CHECK(resultRect.width == 5);
	}

	TEST_CASE("Get intersection values - minus position", "[Rect]")
	{
		nitro::Rect rect1(-10, -10, 10, 10);
		nitro::Rect rect2(-5, -5, 10, 10);


		nitro::Rect resultRect = rect1.intersection(rect2);

		CHECK(resultRect.x_pos == -5);
		CHECK(resultRect.y_pos == -5);
		CHECK(resultRect.height == 5);
		CHECK(resultRect.width == 5);
	}

	TEST_CASE("Get intersection values - full overlap", "[Rect]")
	{
		nitro::Rect rect1(0, 0, 20, 20);
		nitro::Rect rect2(5, 5, 5, 5);


		nitro::Rect resultRect = rect1.intersection(rect2);

		CHECK(resultRect.x_pos == 5);
		CHECK(resultRect.y_pos == 5);
		CHECK(resultRect.height == 5);
		CHECK(resultRect.width == 5);
	}

	TEST_CASE("Get intersection values - intersection does not exist", "[Rect]")
	{
		nitro::Rect rect1(10, 10, 10, 10);
		nitro::Rect rect2(20, 20, 10, 10);

		REQUIRE_THROWS_AS(rect1.intersection(rect2), std::invalid_argument);
	}
}