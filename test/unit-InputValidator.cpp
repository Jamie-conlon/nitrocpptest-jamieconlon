/*
 * unit-InputValidator.cpp
 */

#include "catch.hpp"

#include "InputValidator.h"
#include <string>

namespace nitro_test {

	::nitro::InputValidator iv;
	
	TEST_CASE("No input", "[InputValidator]")
	{
		REQUIRE_THROWS_AS(iv.validate(""), std::invalid_argument);
	}

	TEST_CASE("File doesnt exist", "[InputValidator]")
	{
		REQUIRE_THROWS_AS(iv.validate("/file/doesnt/exist.json"), std::invalid_argument);
	}

	// NOTE: The following two tests won't pass if test file (data/test1.json and data/test1.json) are 
	// not in the correct location relative to where the test is executed.
	// This is not a proper unit due to the relience of an external file, however they will pass if file path is correct. 
	// The file io functions within the validate function should be mocked to pass this unit test properly.

	//TEST_CASE("Not a json file", "[InputValidator]")
	//{
	//	REQUIRE_THROWS_AS(iv.validate("data/test1.txt"), std::invalid_argument);
	//}
	//
	//TEST_CASE("Valid file", "[InputValidator]")
	//{
	//	REQUIRE(iv.validate("data/test1.json") == "data/test1.json");
	//}
}