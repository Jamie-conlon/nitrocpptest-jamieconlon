/*
 * unit-JsonParser.cpp
 */

#include "catch.hpp"
#include "JSONParser.h"
#include "Rect.h"

namespace nitro_test {

	nitro::JSONParser parser;

	TEST_CASE("Can't read json file", "[JSONParser]")
	{
		REQUIRE_THROWS_AS(parser.FileToJSON("not/json/file.json") , std::invalid_argument);
	}

	TEST_CASE("Parse json rects - no rects to parse", "[JSONParser]")
	{
		std::vector<nitro::Rect> result_rects = parser.JSONToRects();
		REQUIRE(result_rects.empty());
	}
}