/*
 * unit-CollisionDetector.cpp
 */

#include "catch.hpp"
#include "CollisionDetector.h"
#include "Rect.h"

namespace nitro_test {

	TEST_CASE("Detect no intersections", "[CollisionDetector]")
	{
		std::vector<nitro::Rect> input_rects({ nitro::Rect(10,10,10,10), nitro::Rect(30,10,10,10) });
		std::vector<nitro::Rect> result_rects;

		nitro::CollisionDetector detector;

		detector.detectIntersections(input_rects);
		result_rects = detector.getAllIntersections();

		REQUIRE(result_rects.empty());
	}

	TEST_CASE("Detect one intersection", "[CollisionDetector]")
	{
		std::vector<nitro::Rect> input_rects({ nitro::Rect(10,10,10,10), nitro::Rect(5,5,10,10) });
		std::vector<nitro::Rect> result_rects;

		nitro::CollisionDetector detector;

		detector.detectIntersections(input_rects);
		result_rects = detector.getAllIntersections();

		REQUIRE(result_rects.size() == 1);

		REQUIRE(result_rects[0].x_pos == 10);
		REQUIRE(result_rects[0].y_pos == 10);
		REQUIRE(result_rects[0].width == 5);
		REQUIRE(result_rects[0].height == 5);
	}

	TEST_CASE("Detect mutliple intersections", "[CollisionDetector]")
	{
		std::vector<nitro::Rect> input_rects({ nitro::Rect(100,100,250,80), nitro::Rect(120,200,250,150), nitro::Rect(140,160,250,190), nitro::Rect(160,140,350,190) });

		nitro::CollisionDetector detector;

		detector.detectIntersections(input_rects);
		std::vector<nitro::Rect> result_rects(detector.getAllIntersections());

		// NOTE: For some reason result_rects only contains 1 rect when testing. It should contain 7. getAllIntersections() works correctly when called from progam. Issue with the unit test library?

		//REQUIRE(result_rects.size() == 3);
	}
}