/*
 * InputValidator.h
 */

#ifndef INPUTVALIDATOR_H
#define INPUTVALIDATOR_H

#include <string>

namespace nitro {

	class InputValidator
	{
	public:
		InputValidator() {}
		~InputValidator() {}

		/* Function: validate(const char *input)
		 *
		 * Takes a c-string from the commmand line and then returns a filename string, if it's a file with a .json extension.
		 */
		std::string validate(const char *input);

	private:
		bool fileExists(const std::string filename);
		bool isJSONFile(const std::string jsonfile);
	};
}
#endif