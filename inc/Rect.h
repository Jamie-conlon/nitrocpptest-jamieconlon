/*
 * Rect.h
 */

#ifndef RECT_H
#define RECT_H

#include <set>

namespace nitro {

	class Rect
	{
	public:
		const int x_pos = 0;
		const int y_pos = 0;
		const int width = 0;
		const int height = 0;
		std::set<int> overlaps;

		/* Constructor: Rect(int x, int y, int w, int h)
		 *
		 * Takes 4 dimensions as arguments and checks if they're valid.
		 * Arguments are valid if they are more than 0 and less than kMaxRectDimension.
		 */
		Rect(int x, int y, int w, int h);
		~Rect() {}

		/* Function: intersects(Rect& rect)
		 *
		 * Checks to see if this rect interests with the augument rect
		 */
		bool intersects(Rect& rect);

		/* Function: intersection(Rect& rect)
         *
         * Finds the intersection between this rect and argument rect.
		 * Returns a Rect of the intersection.
         */
		Rect intersection(Rect& rect);

	private:
		const int kMaxRectDimension = 1000000; // Arbitrary value. 
	};
}
#endif