/*
 * CollisionDetector.h
 */

#ifndef COLLISIONDETECTOR_H
#define COLLISIONDETECTOR_H

#include "Rect.h"
#include <vector>

namespace nitro {

	class CollisionDetector
	{
	public:
		CollisionDetector() {}
		~CollisionDetector() {}

		/* Function: detectIntersections(std::vector<Rect> input_rects)
		 *
		 * - Detects how many Rects intersect with one another in the vector input_rects
		 * - All valid intersecitons are added to the instersect_rects vector.
		 * - This is a recusive function that will continue until input_rects is empty
		 */
		void detectIntersections(std::vector<Rect>& input_rects);

		/* Function: outputIntersections()
		 *
		 * Prints the contents of the intersect_rects vector.
		 */
		void outputIntersections();

		/* Function: getIntersections()
		 *
		 * Returns the vector intersect_rects
		 */
		std::vector<Rect> getAllIntersections();

	private:
		std::vector<Rect> intersect_rects;

		/* Function: checkIfDuplicate(std::set<int> &s1, std::vector<Rect> &rects)
		 *
		 * Checks if a Rect already exits with the same overlaps set. 
		 */
		bool checkIfDuplicate(std::set<int> &s1, std::vector<Rect> &rects);
	};
}
#endif