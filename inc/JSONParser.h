/*
 * JSONParser.h
 */

#ifndef JSONPARSER_H
#define JSONPARSER_H

#include "Rect.h"
#include "json.hpp"
#include <string>

namespace nitro {

	class JSONParser
	{
	public:
		JSONParser() {}
		~JSONParser() {}

		/* Function: FileToJSON(std::string filename)
		 *
		 * Takes a json file name and deserializes the file contents into json_object.
		 */
		void FileToJSON(std::string filename);
		
		/* Function: JSONToRects();
		 *
		 * Looks in json_object for the "rects" object and parses the data inside it.
		 * If a rect has an invalid value, the rect will be skipped and program execution will continue.
		 * Returns a vector of valid Rects.
		 */
		std::vector<Rect> JSONToRects();

		/* Function: OutputRects()
		 *
		 * Prints all of the valid parsed input rects in a specific format.
		 */
		void OutputRects();

	private:
		const unsigned int kMaxRects = 10;
		std::vector<Rect> rects;
		nlohmann::json json_object;
		Rect rectFromJSON(const nlohmann::json &j);
	};
}
#endif